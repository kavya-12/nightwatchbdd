@AT-4
Feature: test

	@TEST_AT-1
	Scenario: test
		Given User opens URL "https://energy.iselect.com.au/electricity/"
		When User enters Postcode as "2000"
		And Select the "2000, SYDNEY" option from the dropdown
		And Click on start
		Then Page title should be "iSelect - Electricity Comparison | Gas Rates | Compare Energy Rates"

